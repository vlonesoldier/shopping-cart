export function getProducts(setProducts: Function): void {
    try {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(res => setProducts(res));
    } catch (err) {
        console.log(err)
    }
}