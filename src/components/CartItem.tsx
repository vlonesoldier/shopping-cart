import { Button, Stack } from "react-bootstrap";
import { useShoppingCart } from "../context/ShoppingCartContext"
import { useEffect, useState } from "react";
import { getProducts } from "../data/getProducts";
import { formatCurrency } from "../utilities/formatCurrency";

type CartItemProps = {
    id: number
    quantity: number
}

export function CartItem({ id, quantity }: CartItemProps) {
    const [products, setProducts] = useState<[]>([]);
    const { removeFromCart } = useShoppingCart();

    useEffect(() => {
        getProducts(setProducts);
    }, [])

    const item = products.find(_item => _item.id === id);
    if (item == null) return null;

    return (
        <Stack direction="horizontal" gap={2} className="d-flex align-items-center">
            <img
                src={item.image}
                style={{
                    width: "150px",
                    height: "100px",
                    objectFit: "cover"
                }} />
            <div className="me-auto">
                <div>
                    {item.title}
                    {quantity > 1 && <span
                        className="text-muted"
                        style={{ fontSize: ".65rem" }}>
                        x{quantity}</span>}
                </div>
                <div className="text-muted" style={{ fontSize: "1rem" }}>
                        {formatCurrency(item.price)}
                </div>
            </div>
            <div>{formatCurrency(item.price * quantity)}</div>
            <Button variant="outline-danger" size="sm" onClick={() => removeFromCart(item.id)}>&times;</Button>
        </Stack>
    )
}
