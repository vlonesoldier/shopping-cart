import { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { StoreItem } from "../components/StoreItem";
import { getProducts } from "../data/getProducts";

export function Store() {
    const [products, setProducts] = useState<[]>([]);

    useEffect(() => {
        getProducts(setProducts);
    }, [])


    return (
        <>
            <h1>Store</h1>
            <Row lg={3} md={2} xs={1} className="g-3">
                {products.map(item =>
                    <Col key={item.id}>
                        <StoreItem {...item} />
                    </Col>)}
            </Row>
        </>
    )
}